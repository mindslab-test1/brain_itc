# MindsLab Inc. 
# Intent Classifier Data Processor

import tensorflow as tf

import time, random, re, json, collections
import numpy as np
from sklearn.metrics import classification_report
from pathlib import Path
from tqdm import tqdm

import component
from component import DataProcessor, InputExample
from trfms.bert_google_tf1 import tokenization
from trfms import utils

class ItcDataProcessor():
    def __init__(self, cfg):
        self.cfg = cfg

        self.model_dir_path = Path(cfg.path.model_dir)

        self.tokenizer = tokenization.FullTokenizer(vocab_file=cfg.path.vocab_file, do_lower_case=cfg.model.do_lower_case)
        self._processor = ItcRawDataProcessor(cfg)
        _, self.label2idx_dict = self._processor.get_labels()
        self.idx2label = { idx:label for label, idx in self.label2idx_dict.items() }

    def build_train_input_fn(self, train_cfg):
        cfg = self.cfg
        model_cfg = cfg.model
        is_training = True      # Now unnecessary, but for future generalization into 'build_input_fn'

        # Load data file into a list of InputExamples
        examples = self._processor.get_file_examples(train_cfg.data, phase='train')

        if is_training:
            seed = int(time.time()) % cfg.seed_bucket
            random.Random(seed).shuffle(examples)

        # Tokenize and write data into a serialized tfrecord file to improve process time
        data_path = Path(train_cfg.data)
        tfrecord_path = data_path.parent.joinpath(model_cfg.tokenizer, data_path.stem).with_suffix('.tfrecord')
        if not tfrecord_path.exists():
            tfrecord_path.parent.mkdir(parents=True, exist_ok=True)

            component.file_based_convert_examples_to_features(
                    examples        = examples, 
                    tokenizer       = self.tokenizer, 
                    output_file     = str(tfrecord_path),
                    max_seq_length  = model_cfg.max_seq_length, 
                    strint_dict     = self.label2idx_dict)

        # Build input_fn using tfrecord
        input_fn = component.file_based_input_fn_builder(
            input_file      = str(tfrecord_path),
            seq_length      = model_cfg.max_seq_length,
            is_training     = is_training,
            drop_remainder  = is_training)

        return input_fn, examples

    # TODO maybe there is a better way 
    def build_infer_fn(self):
        model_cfg = self.cfg.model

        features = {
            'input_ids':   tf.placeholder(tf.int32, shape=[None, model_cfg.max_seq_length], name='input_ids'),
            'input_mask':  tf.placeholder(tf.int32, shape=[None, model_cfg.max_seq_length], name='input_mask'),
            'segment_ids': tf.placeholder(tf.int32, shape=[None, model_cfg.max_seq_length], name='segment_ids'),
            'label_ids':   tf.placeholder(tf.int32, shape=[None], name='label_ids'),
        }
        infer_fn = tf.estimator.export.build_raw_serving_input_receiver_fn(features)

        return infer_fn

    def build_test_examples(self, test_data_file):
        examples = self._processor.get_file_examples(test_data_file, phase='test')
        return examples

    def build_infer_examples(self, infer_texts):
        examples = self._processor.get_infer_examples(infer_texts)
        return examples

    def build_infer_features(self, examples):
        model_cfg = self.cfg.model

        _features = component.convert_examples_to_features(examples, model_cfg.max_seq_length, self.tokenizer, self.label2idx_dict)

        features = {
            'input_ids': [],
            'input_mask': [],
            'segment_ids': [],
            'label_ids': [],
        }
        for feature in _features:
            features['input_ids'].append(feature.input_ids)
            features['input_mask'].append(feature.input_mask)
            features['segment_ids'].append(feature.segment_ids)
            features['label_ids'].append(feature.label_id)

        return features

    def postprocess_batches(self, batched_inputs, batched_outputs):
        cfg = self.cfg

        # extend batched results into concatenated records
        inputs = []
        predictions = []
        for batched_input, batched_output in zip(batched_inputs, batched_outputs):
            probs      = batched_output['probabilities']

            # the order of tuple elements is important. it's for efficiency to choose the tuple structure.
            # it's temporary way of branching between infer(ndim==2), test(ndim==1) 
            if probs.ndim == 2:
                predictions.extend(zip(probs))
                inputs.extend(batched_input)
            elif probs.ndim == 1:
                predictions.append((probs))
                inputs.append(batched_input)

        results = []
        for _input, prediction in zip(inputs, predictions):
            origin_context = _input.text_a
            origin_label_idx = _input.label

            (probs,) = prediction

            top_pred_idxes  = probs.argsort()[::-1][:cfg.infer.num_top_label]
            top_pred_probs  = np.take(probs, top_pred_idxes)
            top_predictions = list(zip(top_pred_idxes, top_pred_probs))
            
            results.append((origin_context, origin_label_idx, top_predictions))
        return results

    def evaluate_result(self, results):
        cfg = self.cfg
        prediction_path = self.model_dir_path.joinpath(cfg.const.prediction)

        labels = []
        preds = []
        with prediction_path.open('w') as f:
            num_written_lines = 0
            score = 0
            for result_idx, result in tqdm(enumerate(results), desc='Writing result'):
                context, label_idx, top_preds = result

                # evaluate
                top_pred_idx, top_pred_prob = top_preds[0]
                preds.append(top_pred_idx)
                labels.append(label_idx)
                score += int(np.equal(top_pred_idx, label_idx))

                # print
                f.write('[#{}] Label: {}({})\n'.format(result_idx+1, label_idx, self.idx2label[label_idx]))
                for pred_idx, pred_prob in top_preds:
                    f.write('Pred: {}({}) {:.2f}%\n'.format(pred_idx, self.idx2label[pred_idx], pred_prob*100))
                f.write('Text: {}\n\n'.format(context))

                num_written_lines += 1

            cm_report = classification_report(labels, preds)
            f.write('\nAcc: {:.4f}\n\nClassification Report\n {}'.format(score / num_written_lines, cm_report))

        print('Acc: {:.4f}'.format(score / num_written_lines))
        print('Classification Report: {}'.format(cm_report))
        assert num_written_lines == len(results)


class ItcRawDataProcessor(DataProcessor):
    """Processor for the ITC data set """
    def __init__(self, cfg):
        self.cfg = cfg
        self._prepare_labels()

    def _prepare_labels(self):
        cfg = self.cfg

        model_dir_path = Path(cfg.path.model_dir)
        label2idx_path = model_dir_path.joinpath(cfg.const.label2idx)
        if not label2idx_path.exists():     # first training phase
            with Path(cfg.train.data).open('r') as f:
                lines = f.readlines()
            labels = set([record.strip().split('\t')[-1] for record in lines])

            label2idx_dict = { label:idx for (idx, label) in enumerate(labels) }
            with label2idx_path.open('w') as f:
                json.dump(label2idx_dict, f)
        else:
            with label2idx_path.open('r') as f:
                label2idx_dict = json.load(f)
            labels = [label for (label, idx) in label2idx_dict.items()]

        self.labels = labels
        self.label2idx_dict = label2idx_dict

    def get_file_examples(self, data_file, phase=None):
        return self._create_examples(
            self._read_tsv(data_file), phase)
    
    def get_infer_examples(self, infer_texts):
        return self._create_examples(
            [(text, None) for text in infer_texts], 'infer')

    def get_labels(self):
        return self.labels, self.label2idx_dict
    
    def _create_examples(self, lines, set_type=None):
        examples = []
        for (i, line) in enumerate(lines):
            guid = "{}-{}".format(set_type, i)
            text_a = tokenization.convert_to_unicode(line[0])
            label = self.label2idx_dict[line[1]] if line[1] in self.label2idx_dict else -1  # -1 for None
            example = InputExample(guid=guid, text_a=text_a, text_b=None, label=label)
            examples.append(example)
        return examples
