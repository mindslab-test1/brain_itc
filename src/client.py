# MindsLab Inc. 
# Intent Classifier grpc client demo

import grpc
import argparse
from proto.itc_v2_pb2 import ItcRequest
from proto.itc_v2_pb2_grpc import IntentClassifierStub

def request(utterance, host, port):
    with grpc.insecure_channel('{}:{}'.format(host, port)) as channel:
        stub = IntentClassifierStub(channel)
        itc_request = ItcRequest(utterance=utterance)

        return stub.GetIntent(itc_request)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', type=str, default="localhost")
    parser.add_argument('--port', type=int, required=True)
    parser.add_argument('--utterance', type=str, default="ITC 테스트용 텍스트입니다. 원하는 텍스트를 입력하세요.")

    args = parser.parse_args()
    
    response = request(args.utterance, args.host, args.port)
    print(response)

    print('[Utterance]:\n {}'.format(args.utterance))
    print('[Predictions]')
    for prediction in response.predictions:
        print('Intent: {}  ({:.2f}%)'.format(prediction.intent, prediction.prob * 100))
