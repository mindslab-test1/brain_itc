# MindsLab Inc. 
# Intent Classifier grpc server

import argparse, logging, sys, time, grpc
from concurrent import futures
from datetime import datetime
from omegaconf import OmegaConf
from pathlib import Path

from runner import ItcRunner, setup_config, setup_logger, logger

from proto.itc_v2_pb2 import IntentPrediction, ItcResponse
from proto.itc_v2_pb2_grpc import IntentClassifierServicer, add_IntentClassifierServicer_to_server

_ONE_DAY_IN_SECONDS = 60 * 60 * 24

class BertITC(IntentClassifierServicer):
    def __init__(self, cfg):
        self.runner = ItcRunner(cfg)

        logger.info('Warming up the GPU with a dummy data')
        _ = self.runner.infer(['Dummy text for warmup'])
        logger.info('Done warming up')

    def GetIntent(self, request, context):
        _input = request.utterance

        results = self.runner.infer([_input])
        logger.debug(results)

        response = self._postprocess(results[0])

        return response

    def _postprocess(self, result):
        predictions = [IntentPrediction(intent=intent, prob=prob) for intent, prob in result]
        output = ItcResponse(predictions=predictions)
        return output


# define and read arguments especially for running, not modeling
def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--model_path', type=str, required=True)
    parser.add_argument('--log_level', type=str, default='INFO', choices=['INFO', 'DEBUG'])

    parser.add_argument('--port', type=int, required=True)

    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = parse_args()
    args.run = 'serve'

    cfg = setup_config(args)
    setup_logger(cfg)

    logger.info('Initializing itc engine')
    bert_itc = BertITC(cfg)

    logger.info('Building grpc server')
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1), )
    add_IntentClassifierServicer_to_server(bert_itc, server)

    server.add_insecure_port('[::]:{}'.format(args.port))
    server.start()

    logger.info('Itc Server starting at 0.0.0.0:{}'.format(args.port))

    try:
        while True:
            # Sleep forever, since `start` doesn't block
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        logger.info('Shutting down the server')
        server.stop(0)
