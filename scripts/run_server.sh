#!/bin/bash

# Generate proto
# bash scripts/build_proto.sh

export CUDA_VISIBLE_DEVICES=6

MODEL_PATH="models/sample"
PORT=35012

python src/server.py \
    --model_path ${MODEL_PATH} \
    --port ${PORT} \
    --log_level "DEBUG"
