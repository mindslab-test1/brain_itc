#!/bin/bash
export CUDA_VISIBLE_DEVICES=6

MODEL_PATH="models/sample"

python src/runner.py --run infer \
    --model_path ${MODEL_PATH} \
    --infer_text "안녕하세요" \
    --infer_text "커피 한잔 주세요" \

