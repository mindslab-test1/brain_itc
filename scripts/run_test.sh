#!/bin/bash
export CUDA_VISIBLE_DEVICES=4

MODEL_PATH="models/sample"

python src/runner.py --run test \
    --model_path ${MODEL_PATH} \
